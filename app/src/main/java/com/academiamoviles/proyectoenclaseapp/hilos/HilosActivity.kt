package com.academiamoviles.proyectoenclaseapp.hilos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.academiamoviles.proyectoenclaseapp.R
import kotlinx.android.synthetic.main.activity_hilos.*
import java.lang.Exception
import java.util.concurrent.Executors

class HilosActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hilos)


        btnTareasPesada.setOnClickListener {

           tareaPesada()
        }

        btnTareaPesadaSegundoPlano.setOnClickListener {

            Executors.newSingleThreadExecutor().execute {

                //Hilo secundario
                tareaPesada()

                runOnUiThread{
                    //Hilo Principal
                    Toast.makeText(this,"Tarea Finalizada",Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

    private fun tareaPesada() {

        try{
            Thread.sleep(5000)
        }catch (ex:Exception){
            ex.toString()
        }
    }
}