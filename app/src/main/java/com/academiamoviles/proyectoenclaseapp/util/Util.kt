package com.academiamoviles.proyectoenclaseapp.util

import android.content.Context
import cn.pedant.SweetAlert.SweetAlertDialog

object Util {

    fun Context.mostrarDialogoRepuesta(titulo:String, contenido:String, itemCallback: (item:SweetAlertDialog)->Unit){

        SweetAlertDialog(this,SweetAlertDialog.SUCCESS_TYPE)
            .setTitleText(titulo)
            .setContentText(contenido)
            .setConfirmText("Continuar")
            .setConfirmClickListener {
                itemCallback(it)
            }.show()
    }

    fun Context.mostrarDialogo(tipo:String, titulo:String, contenido:String) : SweetAlertDialog {

        return when(tipo){

            "SUCCESS" -> {
                SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE).apply {
                    titleText = titulo
                    contentText = contenido
                    show()
                }
            }
            "WARNING" -> {
                SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE).apply {
                    titleText = titulo
                    contentText = contenido
                    show()
                }
            }
            "ERROR" -> {
                SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).apply {
                    titleText = titulo
                    contentText = contenido
                    show()
                }
            }
            else -> {
                SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE).apply {
                    titleText = titulo
                    contentText = contenido
                    show()
                }
            }


        }


    }
}