package com.academiamoviles.proyectoenclaseapp.util

class Constantes {

    companion object {

        const val PREFERENCIA_LOGIN = "preferencia_login"
        const val KEY_EMAIL = "key_email"
        const val KEY_CONTRASENIA = "key_contrasenia"
        const val KEY_ESTADO = "key_estado"
        
    }
}