package com.academiamoviles.proyectoenclaseapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.academiamoviles.proyectoenclaseapp.R
import com.academiamoviles.proyectoenclaseapp.databinding.ActivityMenuBinding

class MenuActivity : AppCompatActivity() {

    lateinit var binding : ActivityMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnEntradas.setOnClickListener {

        }

        binding.btnSegundos.setOnClickListener {


        }

    }
}