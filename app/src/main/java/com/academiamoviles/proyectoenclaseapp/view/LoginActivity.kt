package com.academiamoviles.proyectoenclaseapp.view

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import cn.pedant.SweetAlert.SweetAlertDialog
import com.academiamoviles.proyectoenclaseapp.database.AppDatabase
import com.academiamoviles.proyectoenclaseapp.databinding.ActivityLoginBinding
import com.academiamoviles.proyectoenclaseapp.util.Constantes
import com.academiamoviles.proyectoenclaseapp.util.Util.mostrarDialogo
import java.util.concurrent.Executors

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private val appDatabase by lazy { AppDatabase.obtenerInstancia(this) }
    lateinit var progress: SweetAlertDialog
    private lateinit var encriptar:String
    private lateinit var preferencia: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()

        binding.btnIngresar.setOnClickListener {

            autenticar()
        }

        binding.btnRegistro.setOnClickListener {

            val intent = Intent(this, RegistroActivity::class.java)
            startActivity(intent)
        }
    }

    private fun init() {

        generarEncriptacion()
        recuperarPreferenciaLogin()
    }

    private fun autenticar() {

        progress = this.mostrarDialogo("PROGRESS", "AcademiaMoviles", "ESPERE PORFAVOR")

        val email = binding.edtEmail.text.toString()
        val contrasenia = binding.edtContrasenia.text.toString()

        Executors.newSingleThreadExecutor().execute {

            Thread.sleep(3000)
            val resultado = appDatabase.usuarioDao().autenticar(email, contrasenia)

            if (resultado == 0) {
                progress.dismiss()
                runOnUiThread {
                    this.mostrarDialogo("WARNING", "AcademiaMoviles", "Credenciales Incorrectas")
                }

            } else {
                if (binding.chkRecordarme.isChecked) {
                    guardarPreferenciaLogin(email, contrasenia,1)
                } else {
                    guardarPreferenciaLogin("", "",0)
                }
                irAMenuPrincipal()
            }
        }
    }

    private fun irAMenuPrincipal() {
        val intent = Intent(this, MenuActivity::class.java)
        startActivity(intent)
    }

    private fun guardarPreferenciaLogin(email: String, contrasenia: String, estado:Int) {

        preferencia = EncryptedSharedPreferences.create(Constantes.PREFERENCIA_LOGIN, encriptar,this,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM)

        preferencia?.edit().apply {
            putString(Constantes.KEY_EMAIL, email)
            putString(Constantes.KEY_CONTRASENIA, contrasenia)
            putInt(Constantes.KEY_ESTADO,estado)
            apply()
        }
    }

    private fun recuperarPreferenciaLogin() {

        preferencia = EncryptedSharedPreferences.create(Constantes.PREFERENCIA_LOGIN, encriptar,this,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM)

        val email = preferencia.getString(Constantes.KEY_EMAIL, "")
        val contrasenia = preferencia.getString(Constantes.KEY_CONTRASENIA, "")
        val estado = preferencia.getInt(Constantes.KEY_ESTADO,0)

        binding.edtEmail.setText(email)
        binding.edtContrasenia.setText(contrasenia)

        binding.chkRecordarme.isChecked = estado == 1
    }

    private fun generarEncriptacion(){

        try{
            encriptar = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
        }catch (ex:Exception){
            ex.toString()
        }
    }
}