package com.academiamoviles.proyectoenclaseapp.view

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cn.pedant.SweetAlert.SweetAlertDialog
import com.academiamoviles.proyectoenclaseapp.database.AppDatabase
import com.academiamoviles.proyectoenclaseapp.databinding.ActivityRegistroBinding
import com.academiamoviles.proyectoenclaseapp.model.Usuario
import com.academiamoviles.proyectoenclaseapp.util.Util.mostrarDialogo
import com.academiamoviles.proyectoenclaseapp.util.Util.mostrarDialogoRepuesta
import java.util.concurrent.Executors

class RegistroActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegistroBinding
    private val appDatabase by lazy { AppDatabase.obtenerInstancia(this) }
    private lateinit var progress: SweetAlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegistroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegistro.setOnClickListener {

            try {

                progress = this.mostrarDialogo("PROGRESS", "AcademiaMoviles", "ESPERE PORFAVOR")

                val nombres = binding.edtNombres.text.toString()
                val apellidoPaterno = binding.edtApellidoPaterno.text.toString()
                val apellidoMaterno = binding.edtApellidoMaterno.text.toString()
                val correo = binding.edtCorreo.text.toString()
                val clave = binding.edtClave.text.toString()

                val usuario = Usuario(0, nombres, apellidoPaterno, apellidoMaterno, correo, clave)

                Executors.newSingleThreadExecutor().execute {

                    Thread.sleep(2000)
                    appDatabase.usuarioDao().insertar(usuario)

                    runOnUiThread {
                        progress.dismiss()
                        this.mostrarDialogoRepuesta("Academiamoviles", "Usuario Creado") {
                            onBackPressed()
                        }
                    }
                }
            } catch (ex: Exception) {
                ex.message
            }
        }
    }



}