package com.academiamoviles.proyectoenclaseapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.academiamoviles.proyectoenclaseapp.database.dao.CategoriaDao
import com.academiamoviles.proyectoenclaseapp.database.dao.PlatoDao
import com.academiamoviles.proyectoenclaseapp.database.dao.UsuarioDao
import com.academiamoviles.proyectoenclaseapp.model.Categoria
import com.academiamoviles.proyectoenclaseapp.model.Plato
import com.academiamoviles.proyectoenclaseapp.model.Usuario
import java.util.concurrent.Executors

@Database(entities = [Usuario::class,Plato::class,Categoria::class],version = 1,exportSchema = true)
abstract class AppDatabase : RoomDatabase() {

    //Referenciar todos los daos que tengo en mi aplicacion
    abstract fun usuarioDao(): UsuarioDao
    abstract fun platoDao():PlatoDao
    abstract fun categoriaDao():CategoriaDao

    companion object{
        private var instancia : AppDatabase? = null
        fun obtenerInstancia(context: Context) : AppDatabase{
            if(instancia == null){
                //Crea la base de datos
                instancia = Room.databaseBuilder(
                    context,AppDatabase::class.java,
                    "bdRestaurante")
                    .addCallback(object : RoomDatabase.Callback(){
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)

                            Executors.newSingleThreadExecutor().execute{
                                val usuario = Usuario(0,"init","init","init","init","123")
                                instancia?.usuarioDao()?.insertar(usuario)
                            }

                        }
                    })
                    .build()
            }
            return instancia as AppDatabase
        }
    }
}