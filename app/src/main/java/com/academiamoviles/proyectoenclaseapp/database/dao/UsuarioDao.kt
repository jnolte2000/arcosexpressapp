package com.academiamoviles.proyectoenclaseapp.database.dao

import androidx.room.*
import com.academiamoviles.proyectoenclaseapp.model.Usuario

@Dao
interface UsuarioDao {

    @Insert
    fun insertar(usuario: Usuario)

    @Query("select count(*) from tablaUsuario where correo=:correoInput and clave=:claveInput")
    fun autenticar(correoInput:String,claveInput:String): Int

    //@Update
    //@Delete
}