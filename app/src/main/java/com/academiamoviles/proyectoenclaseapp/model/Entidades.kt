package com.academiamoviles.proyectoenclaseapp.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "tablaUsuario")
data class Usuario(

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "codigo")
    val codigo:Int,

    @ColumnInfo(name = "nombres")
    val nombres:String,

    @ColumnInfo(name = "apellidoPaterno")
    val apellidoPaterno:String,

    @ColumnInfo(name = "apellidoMaterno")
    val apellidoMaterno:String,

    @ColumnInfo(name = "correo")
    val correo:String,

    @ColumnInfo(name = "clave")
    val clave:String

    //@Ignore
    //val nombreCompletos:String
)


@Entity(tableName = "tablaCategoria")
data class Categoria(

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name = "codigo")
    val codigo:Int,

    @ColumnInfo(name = "descripcion")
    val descripcion:String
)

@Entity(tableName = "tablaPlato")
data class Plato(

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name = "codigo")
    val codigo:Int,

    @ColumnInfo(name = "nombrePlato")
    val nombrePlato:String,

    @ColumnInfo(name = "descripcion")
    val descripcion:String,

    @ColumnInfo(name = "precio")
    val precio:Double,

    @ColumnInfo(name = "imagen")
    val imagen:String,

    @ColumnInfo(name = "categoria")
    val categoria:Int
)

